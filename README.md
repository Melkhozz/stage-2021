# Templates vSphere
![Solid](https://zupimages.net/up/21/08/zgwz.png)

*Réalisé par Pierre VALLADE (pierre.vallade@epsi.fr) et Valentin GRIT (valentin.grit@epsi.fr)*

## Introduction
L’objectif de ce document est de détailler les différentes étapes nécessaires à la réalisation de templates au sein de l’hyperviseur vSphere à l’aide des outils Packer (pour la création) et Terraform (pour le déploiement).
On entend par “template” un modèle d’une machine virtuelle, déployable en quelques commandes, avec un système d’exploitation préconfiguré, ainsi que les logiciels désirés préinstallés. 

Ce dépôt est divisé en deux parties : 
- La **création de modèles** via *Packer*, intégrés à l'hyperviseur vSphere
- Le **déploiement** de ces modèles, à l'aide d'un script Python s'appuyant sur *Terraform*

## Fonctionnalités
- Déploiement rapide de *templates* des systèmes d'exploitation suivants :
    - **Linux** : Debian 9, Debian 10, Centos 7
    - **Windows** : Windows 10, Windows Server 2019
-  Création en masse de machines virtuelles personnalisées avec import de fichier CSV

# Création de templates via **Packer**
Chaque template possède sa propre arborescence de fichiers de configuration. 
En prenant pour exemple les templates *Debian*, on compte par exemple les fichiers suivants :

- `debian.json` est le fichier regroupant toutes les caractéristiques d'une machine virtuelle donnée. Il indique la quantité de ressources allouée à la machine, l'infrastructure vSphere à utiliser et où déployer les VM en son sein, des informations sur les différents périphériques (cartes réseaux et disques durs virtuels), ainsi que les différents identifiants à exploiter pour la connexion SSH à la machine, par exemple. La majorité des informations sont cependant substituées par des références au fichier *variables.json*. Ce fichier contient également la commande exécutée au démarrage de l'OS, et permet (entre autres) de préciser l'adresse du fichier de préconfiguration.
- `variables.json` contient toutes les informations précédemment réféfrencées, comme l'adresse de l'image disque (ISO) à utilser, les identifiants vSphere et SSH, ...
- `preseed.cfg` est le fichier de préconfiguration pour Debian. Il permet d'effectuer au préalable tous les choix nécessitant habituellement une réponse interactive via le menu d'installation. On y choisit en autre à l'avance la disposition du clavier, le partitionnement, la création d'utilisateurs, ou encore l'installation de paquets.

Ces fichiers et leurs contenu diffèrent cependant d'un système d'exploitation à l'autre. Pour le modèle de CentOS 7, le *preseed.cfg* sera alors remplacé par le fichier *ks.cfg* ("kickstart configuration"), avec une syntaxe relativement différente. Quant aux modèles pour Windows, le fichier de préconfiguration deviandra alors *autounattended.xml*, et de plusieurs scripts DOS et Powershell seront également inclus, exécutés durant ou à l'issue de l'installation.

Chacun de ces modèles est accompagné d'un script nommé **./start.sh**, permettant d'amorcer la création de chaque template, en nettoyant le cache éventuellement présent sur la machine, puis en démarrant la construction de la template en renseignant les fichiers de configuration nécessaire :
```bash
rm -rf packer_cache/*
packer build -on-error=ask -var-file variables.json windows10.json
```

## Déploiement de templates avec **Terraform**
Les modèles de chaque VM étant désormais présentes au sein du vSphere, leur déploiement est alors ordonnée via le script `terraformCSV.py`, s'appuyant sur l'outil Terraform ainsi qu'un fichier au format **CSV** contenant les informations des VM à déployer. 

Le déploiement en masse de machine virtuelle s'effectue à l'aide de la syntaxe suivante :
`python3 terraformCSV.py [fichier.csv]`

Où le fichier CSV contient les informations dans l'ordre suivant :
|Code-OS|ID-Utilisateur|MdP-Utilisateur|IP-VM|Code-Promo|Classe|Code-Stockage|Reseau|
| - | - | - | - | - | - | - | - |

Chacun de ces champs peuvent accepter les valeurs ci-dessous :
|Champ|Valeurs|
| :-: | - |
|Code-OS|Debian-9, Debian-10, Centos-7, Windows-10, WinServ-2019|
|ID-Utilisateur|Identifiant de l'utilisateur dans le domaine|
|MdP-Utilisateur|Mot de Passe de l'utilisateur dans le domaine|
|IP-VM|Adresse IP à attribuer à la VM|
|Code-Promo|B1, B2, B3, I4, I5 *|
|Classe|1, 2|
|Code-Stockage|Suffixe à `SM-STOCKAGE-` (ex. *B3-2*)|
|Reseau|Nom du réseau *vSphere* à utiliser (ex. *1001-LABO*)|
* *L'arborescence actuelle des dossiers au sein du vSphere n'accepte que la promotion "B2". Idéalement, chaque répertoire de promotion devraient posséder une structure similaire à* B2 : (B2-1) (B2-2) 

Le script va alors, à l'issue de la génération des fichiers de configuration personnalisés, exécuter les commandes suivantes :
```bash
terraform init                  #Mets à jour les fichiers de configuration
terraform fmt                   #Formate les configurations
terraform apply -auto-approve   #Démarre le deploiement sans confirmation interactive
```

